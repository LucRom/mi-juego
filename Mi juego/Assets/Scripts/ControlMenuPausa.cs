using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEditor.ShaderData;
using UnityEngine.SceneManagement;

public class ControlMenuPausa : MonoBehaviour
{
    public static bool pausa = false;
    public GameObject menuPausa;
    void Start()
    {
        menuPausa.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (pausa)
            {
                Continuar();
            }
            else
            {
                Pausa();
            }
        }
    }

    private void Pausa()
    {
        Time.timeScale = 0;
        menuPausa.SetActive(true);
        pausa = true;
    }

    private void Continuar()
    {
        menuPausa.SetActive(false);
        Time.timeScale = 1;
        pausa = false;
    }

    public void BotonContinuar()
    {
        menuPausa.SetActive(false);
        Time.timeScale = 1;
        pausa = false;
    }

    public void MenuPrinciapal()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("Menu");
    }
}
