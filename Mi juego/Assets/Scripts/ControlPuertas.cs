using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPuertas : MonoBehaviour
{
    public bool girarPuertaD = false;
    public bool girarPuertaI = false;

    void Start()
    {
        girarPuertaD = false;
        girarPuertaI = false;
    }
    void Update()
    {
        if (girarPuertaD)
        {
            GirarPuertaD();
        }
        if (girarPuertaI)
        {
            GirarPuertaI();
        }
    }
    private void GirarPuertaD()
    {
        transform.rotation = Quaternion.Euler(0f, -90f, 0f);
        girarPuertaD = false;

    }
    private void GirarPuertaI()
    {
        transform.rotation = Quaternion.Euler(0f, 90f, 0f);
        girarPuertaI = false;

    }
}
