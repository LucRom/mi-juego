using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Video;

public class ControlEnemigoDisparo : MonoBehaviour
{
    private GameObject Jugador;
    private GameObject ZonaDeDeteccion;
    private GameObject gamemanager;

    private GameManager victima;

    public GameObject BalaEnemiga;
    public GameObject Arma;

    public int vidaEDisparo = 100;

    public float velocidad = 0.5f;

    private float tiempo_disparo = 1.3f;

    private bool detectado = false;

    void Start()
    {
        gamemanager = GameObject.Find("Game Manager");
        victima = gamemanager.GetComponent<GameManager>();
        Jugador = GameObject.Find("Jugador");
        ZonaDeDeteccion = GameObject.Find("Area de deteccion D");
    }
    void Update()
    {
        if (detectado)
        {
            tiempo_disparo -= Time.deltaTime;

            if (tiempo_disparo < 0)
            {
                Disparo();
                tiempo_disparo = 1.3f;
            }
            transform.LookAt(Jugador.transform);
            transform.Translate(Vector3.forward * velocidad * Time.deltaTime);

        }

        if (vidaEDisparo == 0)
        {
            victima.cont_testigos++;
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            detectado = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            detectado = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala reflejada"))
        {
            vidaEDisparo = 0;
        }
    }
    private void Disparo()
    {
        GameObject baladisparada;
        baladisparada = Instantiate(BalaEnemiga, Arma.transform.position, Arma.transform.rotation);
        Rigidbody rbbala = baladisparada.GetComponent<Rigidbody>();
        rbbala.AddForce(transform.forward * 14, ForceMode.Impulse);
        Destroy(baladisparada, 0.5f);

    }
}
