using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestShader : MonoBehaviour
{
    public Material shaderescudo;
    private float valor = 0;
    void Start()
    {
        valor = 0;
    }
    void Update()
    {
        valor += Time.deltaTime;
        shaderescudo.SetFloat("_Disolver", valor);
    }
}
