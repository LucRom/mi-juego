using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class GameManager : MonoBehaviour
{
    private GameObject jugador;
    private GameObject fin_del_juego;

    private ControlJugador vidaplayer;

    public TMPro.TMP_Text estado_de_juego;
    public TMPro.TMP_Text Tabparaobjetivo;
    public TMPro.TMP_Text objetivoreciente;

    public int cont_testigos = 0;
    public bool estado = false;

    private List<GameObject> listaObjetos = new List<GameObject>();

    public GameObject enemigoDisparo;
    public GameObject enemigoTorreta;
    public GameObject puertaA;
    public GameObject puertaB;
    public GameObject escudo;

    private float tiemporestante = 3;
    private bool tiempo = false;
    private bool pausamusica = false;

    void Start()
    {
        fin_del_juego = GameObject.Find("FinDeNivel");
        jugador = GameObject.Find("Jugador");
        vidaplayer = jugador.GetComponent<ControlJugador>();
        Reinicio();

    }
    void Update()
    {
        if (estado)
        {
            if(cont_testigos < 2)
            {
                estado_de_juego.text = "SIN TESTIGOS";
                estado_de_juego.color = Color.red;
            }
            else
            {
                Time.timeScale = 0;
                estado_de_juego.text = "Ganaste";
                estado_de_juego.color = Color.green;
            }
        }
        else
        {
            estado_de_juego.text = "";
        }


        if (Input.GetKeyDown(KeyCode.R))
        {
            Reinicio();
        }

        if (vidaplayer.vidajugador == 0)
        {
            Time.timeScale = 0;
            estado_de_juego.text = "R para reinciar";
            estado_de_juego.color = Color.red;
        }
        if (!ControlMenuPausa.pausa)
        {
            if (Input.GetKeyDown(KeyCode.Tab))
            {
                Tabparaobjetivo.text = "-Elimina a todos los enemigos";
                if (cont_testigos == 2)
                {
                    Tabparaobjetivo.text = "Escapa del lugar";
                }
            }
            if (Input.GetKeyUp(KeyCode.Tab))
            {
                Tabparaobjetivo.text = "Tab para ver objetivos";
            }

        }
        else
        {
            pausamusica = true;
            GestorDeAudio.instancia.PausarSonido("Portal Radio");
        }

        if(!ControlMenuPausa.pausa && pausamusica)
        {
            GestorDeAudio.instancia.ReproducirSonido("Portal Radio");
            pausamusica = false;
        }
        if (cont_testigos == 2 && tiempo == false)
        {
            StartCoroutine(ComenzarCronometro());
            tiempo = true;
        }
        if (tiemporestante == 0)
        {
            objetivoreciente.text = "";
        }
    }

    private void Reinicio()
    {
        StopAllCoroutines();
        tiempo = false;
        estado_de_juego.text = "";
        objetivoreciente.text = "";
        cont_testigos = 0;
        GestorDeAudio.instancia.ReproducirSonido("Portal Radio");
        Time.timeScale = 1;
        vidaplayer.vidajugador = 100;
        jugador.transform.position = new Vector3(-12.195f, 4.38f, -3.23f);

        puertaA.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
        puertaB.transform.rotation = Quaternion.Euler(0f, 0f, 0f);

        foreach (GameObject item in listaObjetos)
        {
            Destroy(item);
        }
        listaObjetos.Add(Instantiate(enemigoDisparo, new Vector3(-3.551845f, 3.86f, -3.23f), Quaternion.Euler(0, 90, 0)));
        listaObjetos.Add(Instantiate(enemigoTorreta, new Vector3(-8.036f, 6.29f, -3.23f), Quaternion.Euler(0,90,0)));
        listaObjetos.Add(Instantiate(escudo, new Vector3(-1.06f, 6.001f, -3.186f), Quaternion.Euler(-90, 0, 0)));
    }

    public IEnumerator ComenzarCronometro()
    {

        while (tiemporestante > 0)
        {
            objetivoreciente.text = "�Nuevo objectivo!";
            yield return new WaitForSeconds(1.0f);
            tiemporestante--;
        }
    }

}
