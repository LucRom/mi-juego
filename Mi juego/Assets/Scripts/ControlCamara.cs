using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public GameObject Jugador;
    public Vector3 offset;
    private Camera camara;

    void Start()
    {
        camara = GetComponent<Camera>();
        offset = transform.position - Jugador.transform.position;
    }

    void LateUpdate()
    {
        transform.position = Jugador.transform.position + offset;
    }

    private void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f) 
        {
            camara.fieldOfView++;
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f) 
        {
            camara.fieldOfView--;
        }
    }

}
