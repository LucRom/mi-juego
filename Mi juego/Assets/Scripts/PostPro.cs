using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

public class PostPro : MonoBehaviour
{
    ColorAdjustments ajustecolor;
    void Start()
    {
        Volume volume = GetComponent<Volume>();

        volume.profile.TryGet(out ajustecolor);
        ajustecolor.active = false;
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            ajustecolor.active = true;
        }
        if (Input.GetMouseButtonUp(1))
        {
            ajustecolor.active = false;
        }
    }
}
