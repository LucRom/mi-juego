using System.Collections;
using System.Collections.Generic;
using System.Resources;
using Unity.VisualScripting;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.Assertions.Must;

public class ControlJugador : MonoBehaviour
{
    ControlPuertas controlpuerta_A;
    ControlPuertas controlpuerta_B;

    public GameObject espadasod;
    public GameObject espadasoi;
    public GameObject puertaA;
    public GameObject puertaB;
    public GameObject efectoesudo;
    public Material[] materialesdelescudo;
    public GameObject TeclaE1;

    private GameObject reflejod;
    private GameObject reflejoi;
    private GameObject gamemanager;
    private GameObject escudoefecto;

    private GameManager estado;

    private Renderer renderizadoescudo;

    private Rigidbody rb;

    private int velocidad;
    private int velocidad_sprint = 6;
    private int cant_saltos = 0;

    private float cooldownespadaso = 0f;
    private float tiempodesintegro = 0f;
    private float velocidadcamaralenta;
    private float velocidadnormal;
    private float velocidadfixdeltatime;

    private bool salto = false;
    private bool escudo = false;
    private bool dashd = false;
    private bool dashi = false;
    private bool efectoescudoactivo = false;

    public int vidajugador = 100;

    public char ultimatecla;

    void Start()
    {
        velocidad = 4;
        rb = GetComponent<Rigidbody>();

        reflejod = GameObject.Find("ReflejoD");
        reflejoi = GameObject.Find("ReflejoI");
        controlpuerta_A = puertaA.GetComponent<ControlPuertas>();
        controlpuerta_B = puertaB.GetComponent<ControlPuertas>();
        gamemanager = GameObject.Find("Game Manager");

        estado = gamemanager.GetComponent<GameManager>();

        reflejod.SetActive(false);
        reflejoi.SetActive(false);

        velocidadcamaralenta = 0.5f;
        velocidadnormal = Time.timeScale;
        velocidadfixdeltatime = Time.fixedDeltaTime;
    }

    void Update()
    {
        //Movimiento
        float movimientoHorizontal = Input.GetAxis("Horizontal") * velocidad;

        transform.Translate(new Vector3(movimientoHorizontal, 0f, 0f) * Time.deltaTime);


        if (Input.GetKeyDown(KeyCode.D))
        {
            ultimatecla = 'D';
            reflejod.SetActive(true);
            reflejoi.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            ultimatecla = 'A';
            reflejoi.SetActive(true);
            reflejod.SetActive(false);
        }


        //salto
        if (Input.GetKeyDown(KeyCode.Space))
        {
            salto = true;
        }

        //Sprint
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            velocidad += velocidad_sprint;
        }
        if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            velocidad = 4;
        }

        //Agacharse 
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            transform.localScale = new Vector3(0.62f, 0.5f, 0.62f);
        }
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            transform.localScale = new Vector3(0.62f, 0.6f, 0.62f);
        }

        //Ataque

        if (!ControlMenuPausa.pausa)
        {
            cooldownespadaso -= Time.deltaTime;

            if (Input.GetMouseButtonDown(0))
            {

                if (cooldownespadaso < 0)
                {
                    GestorDeAudio.instancia.ReproducirSonido("Whoosh");
                    Ataque();
                    cooldownespadaso = 1.5f;
                }

            }
        }
 

        //Camaralenta

        if (Input.GetMouseButtonDown(1))
        {
            CamaraLenta();
        }
        if (Input.GetMouseButtonUp(1))
        {
            TerminarCamaraLenta();
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            RaycastHit hit;
            if (ultimatecla == 'D')
            {
                
                if(Physics.Raycast(reflejod.transform.position, Vector3.right,out hit, 2f))
                {
                    if (hit.collider.name == "Puerta A")
                    {
                        controlpuerta_A.girarPuertaD = true;
                    } 
                }
            }
            if (ultimatecla == 'A')
            {
                if (Physics.Raycast(reflejoi.transform.position, Vector3.left, out hit, 2f))
                {
                    if (hit.collider.name == "Puerta B")
                    {
                        controlpuerta_B.girarPuertaI = true;
                    }
                }
            }
        }


        if (escudo)
        {
            vidajugador = 200;
            EfectoEscudo();
            escudo = false;
        }
        if (vidajugador == 100 && efectoescudoactivo)
        {
            tiempodesintegro = 0f;
            renderizadoescudo.sharedMaterial = materialesdelescudo[1];
            materialesdelescudo[1].SetFloat("_Disolver", tiempodesintegro);
            StartCoroutine(TiempoDisolver());
            efectoescudoactivo = false;
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            dashd = true;
        }
        if (Input.GetKeyDown(KeyCode.Q))
        {
            dashi = true;
        }
    }
    private void FixedUpdate()
    {
        if (salto && cant_saltos < 2)
        {
            cant_saltos++;
            rb.AddForce(Vector3.up * 5, ForceMode.Impulse);
            salto = false;
        }

        if (dashd)
        {
            rb.AddForce(Vector3.right * 5, ForceMode.Impulse);
            dashd = false;
        }
        if (dashi)
        {
            rb.AddForce(Vector3.left * 5, ForceMode.Impulse);
            dashi = false;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        cant_saltos = 0;
        if (collision.gameObject.CompareTag("Bala enemiga"))
        {
            vidajugador = vidajugador - 100;
        }
        if (collision.gameObject.CompareTag("Enemigo"))
        {
            vidajugador = vidajugador - 100;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("FinNivel"))
        {
            estado.estado = true;
        }

        if (other.gameObject.CompareTag("Escudo"))
        {
            other.gameObject.SetActive(false);
            escudo = true;
        }

        if (other.gameObject.CompareTag("Escudo"))
        {
            other.gameObject.SetActive(false);
            escudo = true;
        }

        if(other.gameObject.CompareTag("ZonaTecla"))
        {
            GameObject teclaE1;
            teclaE1 = Instantiate(TeclaE1, new Vector3(-9.663f, 5.188f, -4.044f), Quaternion.Euler(90, 90, -90));

            Destroy(teclaE1, 2);
        }


        if (other.gameObject.CompareTag("ZonaTecla1"))
        {
            GameObject teclaE1;
            teclaE1 = Instantiate(TeclaE1, new Vector3(-1.904f, 7.379f, -4.044f), Quaternion.Euler(90, 90, -90));

            Destroy(teclaE1, 2);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("FinNivel"))
        {
            estado.estado = false;
        }
    }

    private void Ataque()
    {
        if (ultimatecla == 'D')
        {

            GameObject ataqued = Instantiate(espadasod, transform.position, espadasod.transform.rotation);
            ataqued.transform.parent = GameObject.Find("Jugador").transform;
            Destroy(ataqued, 1f);
        }
        if (ultimatecla == 'A')
        {

            GameObject ataquei = Instantiate(espadasoi, transform.position, espadasoi.transform.rotation);
            ataquei.transform.parent = GameObject.Find("Jugador").transform;
            Destroy(ataquei, 1f);
        }
    }

    private void CamaraLenta()
    {
        GestorDeAudio.instancia.ReproducirSonido("SMS");
        Time.timeScale = velocidadcamaralenta;
        Time.fixedDeltaTime = velocidadfixdeltatime * velocidadcamaralenta;
    }

    private void TerminarCamaraLenta()
    {
        GestorDeAudio.instancia.ReproducirSonido("SMF");
        Time.timeScale = velocidadnormal;
        Time.fixedDeltaTime = velocidadfixdeltatime;
    }

    private void EfectoEscudo()
    {
        
        escudoefecto = Instantiate(efectoesudo, transform.position, Quaternion.identity);
        escudoefecto.transform.parent = GameObject.Find("Jugador").transform;
        renderizadoescudo = escudoefecto.GetComponent<Renderer>();
        renderizadoescudo.enabled = true;
        renderizadoescudo.sharedMaterial = materialesdelescudo[0];
        efectoescudoactivo = true;
    }

    public IEnumerator TiempoDisolver()
    {
        yield return new WaitForSeconds(1.0f);
        tiempodesintegro = 0f;
        while (tiempodesintegro < 1)
        {
            tiempodesintegro += Time.deltaTime / 2f;
            materialesdelescudo[1].SetFloat("_Disolver", tiempodesintegro);
            yield return null;
            Destroy(escudoefecto, 2);
        }
    }
}
