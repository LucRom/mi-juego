using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class Balas : MonoBehaviour
{
    public GameObject bala_reflejada;
    private GameObject reflejod;
    private GameObject reflejoi;
    private GameObject jugador;
    ControlJugador tecla;
    void Start()
    {
        jugador = GameObject.Find("Jugador");
        reflejod = GameObject.Find("ReflejoD");
        reflejoi = GameObject.Find("ReflejoI");
        tecla = jugador.GetComponent<ControlJugador>();
    }

    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Espadaso"))
        {
            if (tecla.ultimatecla == 'D')
            {
                GameObject bala_ref = Instantiate(bala_reflejada, reflejod.transform.position, Quaternion.identity);
                Rigidbody rbbala = bala_ref.GetComponent<Rigidbody>();

                rbbala.AddForce(transform.right * 14, ForceMode.Impulse);
                Destroy(bala_ref, 1f);
            }
            if (tecla.ultimatecla == 'A')
            {
                GameObject bala_ref = Instantiate(bala_reflejada, reflejoi.transform.position, Quaternion.identity);
                Rigidbody rbbala = bala_ref.GetComponent<Rigidbody>();

                rbbala.AddForce(transform.right * 14, ForceMode.Impulse);
                Destroy(bala_ref, 1f);
            }

        }
    }
}
