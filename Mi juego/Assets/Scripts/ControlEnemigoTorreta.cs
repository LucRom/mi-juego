using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class ControlEnemigoTorreta : MonoBehaviour
{
    private GameObject Jugador;
    private GameObject ZonaDeDeteccion_t;
    private GameObject gamemanager;

    private GameManager victima;

    public GameObject BalaEnemiga;
    public GameObject Arma_t;

    public int vidaET = 100;

    private float tiempo_disparo = 0.5f;

    private bool detectado = false;

    void Start()
    {
        gamemanager = GameObject.Find("Game Manager");
        victima = gamemanager.GetComponent<GameManager>();
        Jugador = GameObject.Find("Jugador");
        ZonaDeDeteccion_t = GameObject.Find("Area de deteccion T");
    }
    void Update()
    {
        if (detectado)
        {
            tiempo_disparo -= Time.deltaTime;

            if (tiempo_disparo < 0)
            {
                Disparo();
                tiempo_disparo = 0.5f;
            }
            transform.LookAt(Jugador.transform);

        }

        if (vidaET == 0)
        {
            victima.cont_testigos++;
            Destroy(gameObject);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            detectado = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Jugador"))
        {
            detectado = false;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala reflejada"))
        {
            vidaET = 0;
        }
    }
    private void Disparo()
    {
        GameObject baladisparada_t = Instantiate(BalaEnemiga, Arma_t.transform.position, Arma_t.transform.rotation);
        Rigidbody rbbala = baladisparada_t.GetComponent<Rigidbody>();

        rbbala.AddForce(transform.forward * 14, ForceMode.Impulse);
    }
}
